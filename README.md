### Intro:

This coding challenge is to asses your skills at the basics of web development through PHP, MySQL and Javascript. The project files must be stored on a github repository for evaluation.

### Task:

1. Using PHP and MySQL, create a program which displays a list of people, displaying their names, ages, and nationality in a table.
2. Sort these people by the alphabetical order of their surnames.
3. Include the ability to add new names, and edit or delete existing ones.
4. Using javascript or JQuery, make use of form validation to disallow numbers in the names field.
5. Again using Javascript or JQuery, make it possible for people to reorder the list by ascending/descending names, ages and nationalities.

### Bonuses:

1. Submit the form via AJAX.
2. Automatically capitalize the names and nationalities columns after submission or before displaying them in the table rows. For example if a user enters 'joe deegan' convert it to 'Joe Deegan'
3. Add hover styles to table rows using css.

### Helpful Reading:
[JS form validation](https://www.w3schools.com/js/js_validation.asp)

[AJAX](https://developer.mozilla.org/en-US/docs/AJAX)